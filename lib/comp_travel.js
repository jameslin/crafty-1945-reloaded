"use strict";

Crafty.c("TravelStraightDown",{
    travel:function(speed){
        this.bind("EnterFrame", function(e){
            this.y = this.y+speed;
            if (this.y > Crafty.viewport.height){
                this.destroy();
            }
        });
        return this;
    }
});

Crafty.c("TravelStraightUp",{
    travel:function(){
        this.bind("EnterFrame", function(e){
            //this.y = this.y-6;
            this.move('n', 6);
            if (this.y < 0){
                this.destroy();
            }
        });
        return this;
    }
});

Crafty.c("TravelAim", {
    travel:function(target){
        this.b = new Crafty.math.Vector2D(this.x+this.width/2, this.y+this.height/2);
        this.t = new Crafty.math.Vector2D(target.x+target.width/2, target.y+target.height/2);
        this.x_diff = (target.x - this.x)*0.02;
        this.y_diff = (target.y - this.y)*0.02;
        this.angle = this.b.angleTo(this.t);
        this.angle = Crafty.math.radToDeg(this.angle) + 90;
        this.attr({rotation:this.angle});
        this.bind("EnterFrame", function(e){
            this.x += this.x_diff;
            this.y += this.y_diff;
            if (this.y < 0 || this.y > Crafty.viewport.height || this.x < 0 || this.x > Crafty.viewport.width){
                this.destroy();
            }
        });
    }
});

Crafty.c("TravelCurve",{
    travel:function(){
        this.number = 0;
        this.operation = 'plus';
        this.bind("EnterFrame", function(e){
            if(this.number > 1){
                this.operation = 'minus';
            }
            if(this.number < 0){
                this.operation = 'plus';
            }
            if (this.operation == 'plus'){
                this.number += 0.01;
            }else{
                this.number -= 0.01;
            }
            this.x += Math.sin(this.number);
            this.y += Math.cos(this.number);
            if (this.y < 0 || this.y > Crafty.viewport.height || this.x < 0 || this.x > Crafty.viewport.width){
                this.destroy();
            }
        });
    }
});
