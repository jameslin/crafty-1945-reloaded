"use strict";

Crafty.c("MouseControl", {
    init:function(){
        this.requires("Mouse");
        this.bind('MouseMove', function(e){
            this.attr({x:e.x-40,y:e.y-40});
        });
        this.bind("EnterFrame", function(e){
            if (e.frame % 10 == 0){
                this.fire();
            }
        })
    },
});

Crafty.c("RandomFire", {
    init:function(){
        this.bind("EnterFrame", function(e){
            if (Crafty.math.randomInt(0, 50) < 1){
                this.fire();
            }
        });
    }
});
