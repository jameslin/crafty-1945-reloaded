Crafty.c("Score", {
    init: function(){
        this.requires("2D, DOM, Text");
        this.attr({x: 120, y:13});
        this.textFont({weight:'bold', size:'15px'});
        this.textColor("#ffffff");
        this.unselectable();
        this.text("<nobr>Score: 0 pts</nobr>");
        this.bind("score", function(score){
            this.text("<nobr>Score: "+score+" pts</nobr>");
            return this;
        });
        return this;
    },
});
