"use strict";
Crafty.c("Weapon", {
    weapon_ammo:function(name){
        this.bullet_types.push(name);
        return this;
    },
    init:function(){
        this.bullet_types = []
        this.tags = []
        return this;
    },
    set_target:function(target){
        this.target = target;
        return this;
    },
    fire:function(){
        for (var i in this.bullet_types){
            Crafty.e(this.bullet_types[i]).attr({x:this.x+18, y:this.y-6}).sound().travel(this.target);
        }
        return this;
    },
});

Crafty.c("Bullet", {
    init:function(){
        this.requires("2D, Canvas, SpriteAnimation");
        return this;
    },
});

Crafty.c("BulletSimple",{
    init:function(){
        this.requires("Bullet, SpriteBulletSimple");
        return this;
    },
    sound:function(){
        //Crafty.audio.play("laser", 1, 0.1);
        return this;
    }
});

Crafty.c("BulletSimpleSmallRound",{
    init:function(){
        this.requires("Bullet, SpriteBulletSmallRound");
        return this;
    },
    sound:function(){
        //Crafty.audio.play("laser", 1, 0.1);
        return this;
    }

});


/*
Crafty.c("BulletLeft",{
    pos: function(x,y){
        this.bullet = Crafty.e("2D, Canvas, SpriteBulletLeft").attr({x:x,y:y})
        return this;
    },
    bullet_fly:function(){
        this.bind("EnterFrame", function(e){
            this.bullet.y = this.bullet.y-6;
            this.bullet.x = this.bullet.x-6;
            if (this.bullet.y < 0 || this.bullet.x < 0){
                this.bullet.destroy();
                this.destroy()
            }
        });
        return this;
    }
});

Crafty.c("BulletRight",{
    pos: function(x,y){
        this.bullet = Crafty.e("2D, Canvas, SpriteBulletRight, PlayerBullet").attr({x:x,y:y})
        return this;
    },
    bullet_fly:function(){
        this.bind("EnterFrame", function(e){
            this.bullet.y = this.bullet.y-6;
            this.bullet.x = this.bullet.x+6;
            if (this.bullet.y < 0 || this.bullet.x > Crafty.viewport.width){
                this.bullet.destroy();
                this.destroy()
            }
        });
        return this;
    }
});
*/
