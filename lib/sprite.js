"use strict";

Crafty.sprite("res/1945.jpg", {
    SpriteBigPlaneYellow: [6,405,63,63],
    SpriteSmallPlaneYellow: [4,4,30,30],
    SpriteBulletSimple: [4,170,30,30],
    SpriteBulletLeft: [4,240,30,25],
    SpriteBulletRight: [38,240,30,25],
    SpriteBulletSmallRound: [275,145,20,20],
    SpriteBigIsland: [170, 500, 60, 60],
});

Crafty.c("Item", {
    init:function(){
        this.is_player = false;
        this.is_enemy = false;
        this.is_background = false;
        return this;
    },
    this_is_player:function(){
        this.is_player = true;
        return this;
    },
    this_is_enemy:function(){
        this.is_enemy = true;
        return this;
    },
    this_is_background:function(){
        this.is_background = true;
        return this;
    }
});

Crafty.c("Plane", {
    init:function(){
        this.requires("2D, Canvas, SpriteAnimation, Item");
        return this;
    },
    score:function(score){
        this.score = score;
        return this;
    },
    health:function(value){
        this.health = value;
        this.total_damage = 0;
        return this;
    },
    hit_by:function(name, damage){
        this.onHit(name, function(){
            var objs = this.hit(name);
            for (var i in objs){
                if (this.health > 0){
                    objs[i].obj.destroy();
                }
            }
            //this.health -= damage;
            this.total_damage += damage;
            if (this.health<this.total_damage){ 
                this.explode();
                if (this.is_enemy){
                    total_score += this.score;
                    Crafty.trigger("score", total_score);
                }
            }
            //health bar
            if (this.is_player){
                var value = (this.total_damage / this.health)*100;
                Crafty.trigger("healthbar", parseInt(value));
            }
        });
        this.bind("AnimationEnd",function(){
            if (this.is_enemy){
                Crafty.audio.play("small_explosion", 1, 0.5);
            }
            this.destroy();
            if (this.is_player){
                Crafty.audio.play("big_explosion", 1, 0.5);
                alert("Game Over\n\nJames Lin Game Development");
                Crafty.scene("main");
            }
        });
        return this;
    },
});

Crafty.c("BigPlaneYellow", {
    init:function(){
        this.requires("Plane, SpriteBigPlaneYellow, Item");
        //set animation
        this.animate("propel", [[4,400],[70,400],[136,400]]);
        this.animate("propel",25,-1);
        return this;
    },
    explode:function(){
        var sequence = [];
        var space = 67;
        var x = 5;
        var y = 303;
        for (var i=0; i<2; i++){
            sequence.push([x, y]);
            x += space;
        }
        this.animate("explode", sequence).animate("explode", 10, 0);
    },
});

Crafty.c("SmallPlaneYellow", {
    init:function(){
        this.requires("Plane, SpriteSmallPlaneYellow");
        return this;
    },
    explode:function(){
        var sequence = [];
        var space = 34;
        var x = 70;
        var y = 170;
        for (var i=0; i<4; i++){
            sequence.push([x, y]);
            x += space;
        }
        this.animate("explode", sequence).animate("explode", 10, 0);
    }
});

Crafty.c("BackgroundItem", {
    init:function(){
        this.requires("2D, Canvas, SpriteAnimation, Item");
        this.this_is_background();
        return this;
    },
});

Crafty.c("BigIsland", {
    init:function(){
        this.requires("BackgroundItem, SpriteBigIsland");
        return this;
    }
});
